package evaluator.controller;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;
import evaluator.model.Test;
import evaluator.repository.IntrebariRepository;

import java.util.LinkedList;
import java.util.List;

public class AppController {

    private IntrebariRepository intrebariRepository;

    public AppController() {
        intrebariRepository = new IntrebariRepository();
    }

    public Intrebare addNewIntrebare(Intrebare intrebare) throws DuplicateIntrebareException {

        intrebariRepository.addIntrebare(intrebare);

        return intrebare;
    }

    public boolean exists(Intrebare intrebare) {
        return intrebariRepository.exists(intrebare);
    }

    public Test createNewTest() throws NotAbleToCreateTestException {

        /*1*/if (intrebariRepository.getIntrebari().size() < 5)
            /*2*/ throw new NotAbleToCreateTestException("Nu exista suficiente intrebari pentru crearea unui test!(5)");

        /*3*/if (intrebariRepository.getNumberOfDistinctDomains() < 5)
            /*4*/ throw new NotAbleToCreateTestException("Nu exista suficiente domenii pentru crearea unui test!(5)");

        /*5*/ List<Intrebare> testIntrebari = new LinkedList<Intrebare>();
        List<String> domenii = new LinkedList<String>();
        Intrebare intrebare;
        Test test = new Test();

        /*6 */while (testIntrebari.size() != 5) {
            /*7*/ intrebare = intrebariRepository.pickRandomIntrebare();

            /*8*/ if (!testIntrebari.contains(intrebare) && !domenii.contains(intrebare.getDomeniu())) {
                /*9*/  testIntrebari.add(intrebare);
                domenii.add(intrebare.getDomeniu());
            }
        }

        /*10*/  test.setIntrebari(testIntrebari);
        /*11*/ return test;

    }

    public void loadIntrebariFromFile(String f) {
        List<Intrebare> intrebari = intrebariRepository.loadIntrebariFromFile(f);
        if (intrebari != null) {
            intrebariRepository.setIntrebari(intrebari);
        } else {
            System.out.println("Nu se poate incarca din fisier!");
        }
    }

    public Statistica getStatistica() throws NotAbleToCreateStatisticsException {

        if (intrebariRepository.getIntrebari().isEmpty())
            throw new NotAbleToCreateStatisticsException("Repository-ul nu contine nicio intrebare!");

        Statistica statistica = new Statistica();
        for (String domeniu : intrebariRepository.getDistinctDomains()) {
            statistica.add(domeniu, intrebariRepository.getNumberOfIntrebariByDomain(domeniu));
        }

        return statistica;
    }

}
