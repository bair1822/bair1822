package evaluator.main;

import evaluator.controller.AppController;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

//functionalitati
//i.	 adaugarea unei noi intrebari pentru un anumit domeniu (enunt intrebare, raspuns 1, raspuns 2, raspuns 3, raspunsul corect, domeniul) in setul de intrebari disponibile;
//ii.	 crearea unui nou test (testul va contine 5 intrebari alese aleator din cele disponibile, din domenii diferite);
//iii.	 afisarea unei statistici cu numarul de intrebari organizate pe domenii.

public class StartApp {

    private static final String file = "C:\\05-ProiectEvaluatorExamen\\ProiectEvaluatorExamen\\src\\main\\java\\intrebari.txt";

    private static Intrebare creareIntrebare() {

        Scanner keyboard = new Scanner(System.in);
        System.out.println("Introduceti enuntul cu litera mare si semn de intrebare:");
        String enunt = keyboard.next();
        System.out.println("Introduceti raspuns1 cu 1) inainte:");
        String raspuns1 = keyboard.next();
        System.out.println("Introduceti raspuns2 cu 2) inainte:");
        String raspuns2 = keyboard.next();
        System.out.println("Introduceti raspuns3 cu 3):");
        String raspuns3 = keyboard.next();
        System.out.println("Introduceti raspunsul corect 1|| 2|| 3:");
        String raspunsCorect = keyboard.next();
        System.out.println("Introduceti domeniul cu litera mare:");
        String domeniu = keyboard.next();

        try {
            return new Intrebare(enunt, raspuns1, raspuns2, raspuns3, raspunsCorect, domeniu);
        } catch (InputValidationFailedException e) {
            System.out.println(e);
            return null;
        }
    }

    public static void main(String[] args) throws IOException {

        BufferedReader console = new BufferedReader(new InputStreamReader(System.in));

        AppController appController = new AppController();

        boolean activ = true;
        String optiune = null;

        while (activ) {

            System.out.println("");
            System.out.println("1.Adauga intrebare");
            System.out.println("2.Creeaza test");
            System.out.println("3.Statistica");
            System.out.println("4.Exit");
            System.out.println("");

            optiune = console.readLine();

            switch (optiune) {
                case "1":
                    Intrebare intrebare = creareIntrebare();
                    if (null != intrebare) {
                        try {
                            appController.addNewIntrebare(intrebare);
                        } catch (DuplicateIntrebareException e) {
                            System.out.println("Eroare intrebare duplicata! ");
                        }
                    }

                case "2":
                    try {
                        appController.createNewTest();
                    } catch (NotAbleToCreateTestException e) {
                        System.out.println("Not able to create a new test!\n"+e.getMessage());
                    }
                    break;
                case "3":
                    appController.loadIntrebariFromFile(file);
                    Statistica statistica;
                    try {
                        statistica = appController.getStatistica();
                        System.out.println(statistica);
                    } catch (NotAbleToCreateStatisticsException e) {
                        System.out.println("ERROR: NotAbleToCreateStatistics");
                    }

                    break;
                case "4":
                    activ = false;
                    break;
                default:
                    break;
            }
        }

    }

}
