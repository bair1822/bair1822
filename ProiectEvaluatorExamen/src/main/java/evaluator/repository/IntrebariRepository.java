package evaluator.repository;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.model.Intrebare;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class IntrebariRepository extends InputValidationFailedException {

    private List<Intrebare> intrebari;

    public IntrebariRepository() {
        setIntrebari(new LinkedList<Intrebare>());
    }

    public void addIntrebare(Intrebare i) throws DuplicateIntrebareException {
        if (exists(i))
            throw new DuplicateIntrebareException("Intrebarea deja exista!");
        intrebari.add(i);
    }

    public void addIntrebare(String enunt, String varianta1, String varianta2, String varianta3, String variantaCorecta, String domeniu) throws DuplicateIntrebareException, InputValidationFailedException {
        Intrebare i = new Intrebare(enunt, varianta1, varianta2, varianta3, variantaCorecta, domeniu);
        if (exists(i))
            throw new DuplicateIntrebareException("Intrebarea deja exista!");
        intrebari.add(i);
    }

    public boolean exists(Intrebare i) {
        for (Intrebare intrebare : intrebari)
            if (intrebare.equals(i))
                return true;
        return false;
    }

    public Intrebare pickRandomIntrebare() {
        Random random = new Random();
        return intrebari.get(random.nextInt(intrebari.size()));
    }

    public int getNumberOfDistinctDomains() {
        return getDistinctDomains().size();

    }

    public Set<String> getDistinctDomains() {
        Set<String> domains = new TreeSet<String>();
        for (Intrebare intrebre : intrebari)
            domains.add(intrebre.getDomeniu());
        return domains;
    }

    public List<Intrebare> getIntrebariByDomain(String domain) {
        List<Intrebare> intrebariByDomain = new LinkedList<Intrebare>();
        for (Intrebare intrebare : intrebari) {
            if (intrebare.getDomeniu().equals(domain)) {
                intrebariByDomain.add(intrebare);
            }
        }

        return intrebariByDomain;
    }

    public int getNumberOfIntrebariByDomain(String domain) {
        return getIntrebariByDomain(domain).size();
    }

    public List<Intrebare> loadIntrebariFromFile(String f) {

        List<Intrebare> intrebari = new LinkedList<Intrebare>();
        BufferedReader br = null;
        String line;
        List<String> intrebareAux;
        Intrebare intrebare;

        try {
            br = new BufferedReader(new FileReader(f));
            line = br.readLine();
            while (line != null) {
                intrebareAux = new LinkedList<>();
                while (!line.equals("##")) {
                    intrebareAux.add(line);
                    line = br.readLine();
                }
                intrebare = new Intrebare();
                intrebare.setEnunt(intrebareAux.get(0));
                intrebare.setVarianta1(intrebareAux.get(1));
                intrebare.setVarianta2(intrebareAux.get(2));
                intrebare.setVarianta3(intrebareAux.get(3));
                intrebare.setVariantaCorecta(intrebareAux.get(4));
                intrebare.setDomeniu(intrebareAux.get(5));
                intrebari.add(intrebare);
                line = br.readLine();
            }

        } catch (IOException e) {
            System.out.println("Eroare la citirea din fisier");
            return null;
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException e) {
                System.out.println("Eroare la inchiderea fisierului");
            }
        }

        return intrebari;
    }

    public List<Intrebare> getIntrebari() {
        return intrebari;
    }

    public void setIntrebari(List<Intrebare> intrebari) {
        this.intrebari = intrebari;
    }

}
