package evaluator.controller;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AppControllerTest {

    AppController appController = new AppController();


    @Test
    public void shouldCreateNewTest() throws InputValidationFailedException, DuplicateIntrebareException, NotAbleToCreateTestException {
        appController.addNewIntrebare(new Intrebare("Enunt?", "1)da", "2)nu", "3)poate", "3", "Domeniu"));
        appController.addNewIntrebare(new Intrebare("Cat fac 2+2?", "1)4", "2)5", "3)2*2", "1", "Bitul ascuns"));
        appController.addNewIntrebare(new Intrebare("Enunt2?", "1)da", "2)nu", "3)poate", "3", "Domeniu2"));
        appController.addNewIntrebare(new Intrebare("Enunt3?", "1)da", "2)nu", "3)poate", "3", "Domeniu3"));
        appController.addNewIntrebare(new Intrebare("Enunt4?", "1)da", "2)nu", "3)poate", "3", "Domeniu4"));
        evaluator.model.Test newTest = appController.createNewTest();
        assertEquals(newTest.getIntrebari().size(), 5);
    }

    @Test
    public void shouldNotCreateNewTestNotEnoughtDomenii() throws InputValidationFailedException, DuplicateIntrebareException {
        appController.addNewIntrebare(new Intrebare("Enunt?", "1)da", "2)nu", "3)poate", "3", "Domeniu"));
        appController.addNewIntrebare(new Intrebare("Cat fac 2+2?", "1)4", "2)5", "3)2*2", "1", "Bitul ascuns"));
        appController.addNewIntrebare(new Intrebare("Enunt2?", "1)da", "2)nu", "3)poate", "3", "Domeniu2"));
        appController.addNewIntrebare(new Intrebare("Enunt3?", "1)da", "2)nu", "3)poate", "3", "Domeniu3"));
        appController.addNewIntrebare(new Intrebare("Enunt4?", "1)da", "2)nu", "3)poate", "3", "Domeniu"));
        try {
            appController.createNewTest();
            assert false;
        } catch (NotAbleToCreateTestException exception) {
            assertEquals(exception.getMessage(), "Nu exista suficiente domenii pentru crearea unui test!(5)");
            assert true;
        }
    }


    @Test
    public void shouldNotCreateNewTestNotEnoughtIntrebari() throws InputValidationFailedException, DuplicateIntrebareException {
        appController.addNewIntrebare(new Intrebare("Enunt4?", "1)da", "2)nu", "3)poate", "3", "Domeniu"));
        try {
            appController.createNewTest();
            assert false;
        } catch (NotAbleToCreateTestException exception) {
            assertEquals(exception.getMessage(), "Nu exista suficiente intrebari pentru crearea unui test!(5)");
            assert true;
        }
    }

    @Test
    public void shouldGetStatisticaTest() throws InputValidationFailedException, DuplicateIntrebareException {
        appController.addNewIntrebare(new Intrebare("Enunt?", "1)da", "2)nu", "3)poate", "3", "Domeniu"));
        appController.addNewIntrebare(new Intrebare("Cat fac 2+2?", "1)4", "2)5", "3)2*2", "1", "Bitul ascuns"));
        appController.addNewIntrebare(new Intrebare("Enunt2?", "1)da", "2)nu", "3)poate", "3", "Domeniu2"));
        appController.addNewIntrebare(new Intrebare("Enunt3?", "1)da", "2)nu", "3)poate", "3", "Domeniu3"));
        appController.addNewIntrebare(new Intrebare("Enunt4?", "1)da", "2)nu", "3)poate", "3", "Domeniu4"));
        try {
            Statistica statistica = appController.getStatistica();
            assertEquals(statistica.getIntrebariDomenii().size(), 5);
        } catch (NotAbleToCreateStatisticsException e) {
            assert false;
        }
    }

    @Test(expected = NotAbleToCreateStatisticsException.class)
    public void shouldFailToGetStatisticaTest() throws NotAbleToCreateStatisticsException {
        Statistica statistica = appController.getStatistica();

    }



}