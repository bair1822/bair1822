package evaluator;

import evaluator.controller.AppController;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;
import evaluator.repository.IntrebariRepository;
import junit.framework.TestCase;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class IntegrationTest {
    AppController appController = new AppController();
    evaluator.model.Test newTest;
    IntrebariRepository intrebariRepository = new IntrebariRepository();


    @Test
    public void shouldAddIntrebare() throws InputValidationFailedException, DuplicateIntrebareException {
        TestCase.assertEquals(0, intrebariRepository.getIntrebari().size());
        intrebariRepository.addIntrebare(new Intrebare("Cat fac 2+2?", "1)4", "2)5", "3)2*2", "1", "Bitul ascuns"));
        TestCase.assertEquals(1, intrebariRepository.getIntrebari().size());
        TestCase.assertEquals(new Intrebare("Cat fac 2+2?", "1)4", "2)5", "3)2*2", "1", "Bitul ascuns"), intrebariRepository.getIntrebari().get(0));
    }

    @Test
    public void shouldCreateNewTest() throws InputValidationFailedException, DuplicateIntrebareException, NotAbleToCreateTestException {
        appController.addNewIntrebare(new Intrebare("Enunt?", "1)da", "2)nu", "3)poate", "3", "Domeniu"));
        appController.addNewIntrebare(new Intrebare("Cat fac 2+2?", "1)4", "2)5", "3)2*2", "1", "Bitul ascuns"));
        appController.addNewIntrebare(new Intrebare("Enunt2?", "1)da", "2)nu", "3)poate", "3", "Domeniu2"));
        appController.addNewIntrebare(new Intrebare("Enunt3?", "1)da", "2)nu", "3)poate", "3", "Domeniu3"));
        appController.addNewIntrebare(new Intrebare("Enunt4?", "1)da", "2)nu", "3)poate", "3", "Domeniu4"));
        newTest = appController.createNewTest();
        assertEquals(newTest.getIntrebari().size(), 5);
    }

    @Test
    public void shouldGetStatisticaTest() throws InputValidationFailedException, DuplicateIntrebareException {
        try {
            Statistica statistica = appController.getStatistica();
            assertEquals(statistica.getIntrebariDomenii().size(), 5);
        } catch (NotAbleToCreateStatisticsException e) {
            assert false;
        }
    }

    @Test
    public void integrationTest() throws InputValidationFailedException, DuplicateIntrebareException, NotAbleToCreateTestException, NotAbleToCreateStatisticsException {
        appController.addNewIntrebare(new Intrebare("Enunt?", "1)da", "2)nu", "3)poate", "3", "Domeniu"));
        appController.addNewIntrebare(new Intrebare("Cat fac 2+2?", "1)4", "2)5", "3)2*2", "1", "Bitul ascuns"));
        appController.addNewIntrebare(new Intrebare("Enunt2?", "1)da", "2)nu", "3)poate", "3", "Domeniu2"));
        appController.addNewIntrebare(new Intrebare("Enunt3?", "1)da", "2)nu", "3)poate", "3", "Domeniu3"));
        appController.addNewIntrebare(new Intrebare("Enunt4?", "1)da", "2)nu", "3)poate", "3", "Domeniu4"));
        evaluator.model.Test newTest = appController.createNewTest();
        appController.getStatistica();
    }


}
