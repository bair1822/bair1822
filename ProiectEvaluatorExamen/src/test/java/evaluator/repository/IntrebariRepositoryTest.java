package evaluator.repository;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.model.Intrebare;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class IntrebariRepositoryTest {
    IntrebariRepository intrebariRepository = new IntrebariRepository();


    @Test
    public void shouldAddIntrebare() throws InputValidationFailedException, DuplicateIntrebareException {
        assertEquals(0, intrebariRepository.getIntrebari().size());
        intrebariRepository.addIntrebare(new Intrebare("Cat fac 2+2?", "1)4", "2)5", "3)2*2", "1", "Bitul ascuns"));
        assertEquals(1, intrebariRepository.getIntrebari().size());
        assertEquals(new Intrebare("Cat fac 2+2?", "1)4", "2)5", "3)2*2", "1", "Bitul ascuns"), intrebariRepository.getIntrebari().get(0));
    }

    @Test
    public void shouldFailEnuntCheck() throws DuplicateIntrebareException {
        assertEquals(0, intrebariRepository.getIntrebari().size());
        try {
            intrebariRepository.addIntrebare(new Intrebare("cat fac 2+2?", "1)4", "2)5", "3)2*2", "1", "Bitul ascuns"));
            assert false;
        } catch (InputValidationFailedException e) {
            assertEquals("Prima litera din enunt nu e majuscula!", e.getMessage());
            assertEquals(0, intrebariRepository.getIntrebari().size());
            assert true;
        }
    }

    @Test
    public void shouldFailEnuntCheckNoQuestionMark() throws DuplicateIntrebareException {
        assertEquals(0, intrebariRepository.getIntrebari().size());
        try {
            intrebariRepository.addIntrebare(new Intrebare(" Cat fac 2+2", "1)4", "2)5", "3)2*2", "1", "Bitul ascuns"));
            assert false;
        } catch (InputValidationFailedException e) {
            assertEquals("Ultimul caracter din enunt nu e '?'!", e.getMessage());
            assertEquals(0, intrebariRepository.getIntrebari().size());
            assert true;
        }

    }

    @Test
    public void shouldFailVarianta1Check() throws DuplicateIntrebareException {
        assertEquals(0, intrebariRepository.getIntrebari().size());
        try {
            intrebariRepository.addIntrebare(new Intrebare("Cat fac 2+2?", "1", "2)5", "3)2*2", "1", "Bitul ascuns"));
            assert false;
        } catch (InputValidationFailedException e) {
            assertEquals("Varianta nu incepe cu '1)'!", e.getMessage());
            assertEquals(0, intrebariRepository.getIntrebari().size());
            assert true;
        }
    }

    @Test
    public void shouldFailVarianta2LengthCheck() throws DuplicateIntrebareException {
        assertEquals(0, intrebariRepository.getIntrebari().size());
        try {
            intrebariRepository.addIntrebare(new Intrebare("Cat fac 2+2?", "1)", "2)variantadinaialungacinestasaciteascaasemeneavariantanuarecefacedaracumeuchiarscriuasta", "3)2*2", "1", "Bitul ascuns"));
            assert false;
        } catch (InputValidationFailedException e) {
            assertEquals("Lungimea variantei2 depaseste 50 de caractere!", e.getMessage());
            assertEquals(0, intrebariRepository.getIntrebari().size());
            assert true;
        }
    }

    @Test
    public void shouldFailVarianta3Check() throws DuplicateIntrebareException {
        assertEquals(0, intrebariRepository.getIntrebari().size());
        try {
            intrebariRepository.addIntrebare(new Intrebare("Cat fac 2+2?", "1)", "2)5", "", "1", "Bitul ascuns"));
            assert false;
        } catch (InputValidationFailedException e) {
            assertEquals("Varianta3 este vida!", e.getMessage());
            assertEquals(0, intrebariRepository.getIntrebari().size());
            assert true;
        }
    }

    @Test
    public void shouldFailVariantaCorectaCheck() throws DuplicateIntrebareException {
        assertEquals(0, intrebariRepository.getIntrebari().size());
        try {
            intrebariRepository.addIntrebare(new Intrebare("Cat fac 2+2?", "1)1", "2)varianta2", "3)2*2", "4", "Bitul ascuns"));
            assert false;
        } catch (InputValidationFailedException e) {
            assertEquals("Varianta corecta nu este unul dintre caracterele {'1', '2', '3'}", e.getMessage());
            assertEquals(0, intrebariRepository.getIntrebari().size());
        }
    }

    @Test
    public void shouldFailDuplicateIntrebareException() throws InputValidationFailedException {
        assertEquals(0, intrebariRepository.getIntrebari().size());
        try {
            intrebariRepository.addIntrebare(new Intrebare("Cat fac 2+2?", "1)4", "2)5", "3)2*2", "1", "Bitul ascuns"));
        } catch (DuplicateIntrebareException e) {
            assert false;
        }
        assertEquals(1, intrebariRepository.getIntrebari().size());
        try {
            intrebariRepository.addIntrebare(new Intrebare("Cat fac 2+2?", "1)4", "2)5", "3)2*2", "1", "Bitul ascuns"));
        } catch (DuplicateIntrebareException e) {
            assertEquals("Intrebarea deja exista!", e.getMessage());
            assertEquals(1, intrebariRepository.getIntrebari().size());
            assert true;
        }
    }

    @Test
    public void shouldFailDomeniuCheck() throws DuplicateIntrebareException {
        assertEquals(0, intrebariRepository.getIntrebari().size());
        try {
            intrebariRepository.addIntrebare(new Intrebare("Cat fac 2+2?", "1)1", "2)varianta2", "3)2*2", "3", "bitul ascuns"));
            assert false;
        } catch (InputValidationFailedException e) {
            assertEquals("Prima litera din domeniu nu e majuscula!", e.getMessage());
            assertEquals(0, intrebariRepository.getIntrebari().size());
        }
    }

    @Test
    public void shouldFailDomeniuFaraMajusculaCheck() throws DuplicateIntrebareException {
        assertEquals(0, intrebariRepository.getIntrebari().size());
        try {
            intrebariRepository.addIntrebare(new Intrebare("Cat fac 2+2?", "1)1", "2)varianta2", "3)2*2", "3", ""));
            assert false;
        } catch (InputValidationFailedException e) {
            assertEquals("Domeniul este vid!", e.getMessage());
            assertEquals(0, intrebariRepository.getIntrebari().size());
        }
    }

    @Test
    public void shouldFailDomeniuLengthCheck() throws DuplicateIntrebareException {
        assertEquals(0, intrebariRepository.getIntrebari().size());
        try {
            intrebariRepository.addIntrebare(new Intrebare("Cat fac 2+2?", "1)1", "2)varianta2", "3)2*2", "3", "Astadadomeniulungcinemaistiedeundeamplecatsaridiceomana"));
            assert false;
        } catch (InputValidationFailedException e) {
            assertEquals("Lungimea domeniului depaseste 30 de caractere!", e.getMessage());
            assertEquals(0, intrebariRepository.getIntrebari().size());
        }
    }
}
